Rails.application.routes.draw do
  resources :movies, only: [:index, :show]
  resources :users, only: [:create]
  post 'auth/login', to: 'authentications#create'
end
