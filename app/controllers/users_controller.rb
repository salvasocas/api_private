class UsersController < ApplicationController
  def create
    user = User.new(user_params)
    if user.save
      render json: user.to_json(only: [:nickname, :email])
    else
      render json: user.errors.messages, status: 400
    end
  end

  private

  def user_params
    params.require(:user).permit(:nickname, :email, :password)
  end
end
